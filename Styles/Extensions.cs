﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GuitarHeroDrums.Styles
{
    public static class Extensions
    {
        public static readonly DependencyProperty MouseDoubleClickCommand = EventBehaviourFactory.CreateCommandExecutionEventBehaviour(ListView.MouseDoubleClickEvent, "MouseDoubleClickCommand", typeof(Extensions), CommandParameterModes.RoutedEventOriginalSourceDataContext);

        public static void SetMouseDoubleClickCommand(DependencyObject o, ICommand value)
        {
            o.SetValue(MouseDoubleClickCommand, value);
        }

        public static ICommand GetMouseDoubleClickCommand(DependencyObject o)
        {
            return o.GetValue(MouseDoubleClickCommand) as ICommand;
        }
    }
}
