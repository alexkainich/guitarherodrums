﻿using System.Linq;
using System.Windows.Input;
using System.Collections.Generic;

namespace GuitarHeroDrums.ViewModels
{
    public class MessagesViewModel : ObservableObject
    {
        #region Fields
        private IPageViewModel _currentPageViewModel;
        private List<IPageViewModel> _pageViewModels;
        #endregion

        public MessagesViewModel()
        {
            PageViewModels.Add(new ReceivedViewModel(this));
            PageViewModels.Add(new SavedViewModel(this));

            CurrentPageViewModel = PageViewModels[0];
        }

        #region Properties / Commands
        private ICommand changePageCommand;
        public ICommand ChangePageCommand
        {
            get
            {
                return this.changePageCommand ?? (this.changePageCommand = new RelayCommand<IPageViewModel>(this.executeChangePageCommand, this.canChangePageCommand));
            }
        }

        private bool canChangePageCommand(IPageViewModel pageViewModel)
        {
            return true;
        }

        private void executeChangePageCommand(IPageViewModel pageViewModel)
        {
            ChangeViewModel(pageViewModel);

            PageViewModels[0].UpdateButtonBackground();
            PageViewModels[1].UpdateButtonBackground();
        }

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (_pageViewModels == null)
                    _pageViewModels = new List<IPageViewModel>();

                return _pageViewModels;
            }
        }

        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return _currentPageViewModel;
            }
            set
            {
                if (_currentPageViewModel != value)
                {
                    _currentPageViewModel = value;
                    OnPropertyChanged("CurrentPageViewModel");
                }
            }
        }
        #endregion

        #region Methods
        private void ChangeViewModel(IPageViewModel viewModel)
        {
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);

            CurrentPageViewModel = PageViewModels
                .FirstOrDefault(vm => vm == viewModel);
        }
        #endregion
    }
}
