﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using NAudio.Midi;

namespace GuitarHeroDrums.ViewModels
{
    public class LogViewModel : ObservableObject
    {
        #region Fields
        private string _logTextBoxText;
        #endregion Fields

        public LogViewModel()
        {

        }

        #region Properties / Commands
        public bool DisableLog { get; set; }
        public string LogTextBoxText
        {
            get { return _logTextBoxText; }
            set
            {
                if (_logTextBoxText != value)
                {
                    _logTextBoxText = value;

                    //if log larger than 0.0625mb then remove first 100 lines
                    if (_logTextBoxText.Length > 31250)
                    {
                        _logTextBoxText = RemoveFirstLines(_logTextBoxText);
                    }

                    this.OnPropertyChanged("LogTextBoxText");
                }
            }
        }

        private ICommand clearLogCommand;
        public ICommand ClearLogCommand
        {
            get
            {
                return this.clearLogCommand ?? (this.clearLogCommand = new RelayCommand(this.executeClearLogCommand, this.canClearLogCommand));
            }
        }

        private bool canClearLogCommand()
        {
            return true;
        }

        private void executeClearLogCommand()
        {
            try
            {
                LogTextBoxText = "";

                this.OnPropertyChanged("LogTextBoxText");
            }
            catch
            {

            }
        }
        #endregion Properties / Commands

        #region Methods
        public static string RemoveFirstLines(string text)
        {
            var allLines = Regex.Split(text, "\r\n");

            int linesCount = Int32.Parse(Math.Round(allLines.Count() / 2d).ToString());
            var lines = allLines.Skip(linesCount);

            return string.Join(Environment.NewLine, lines.ToArray());
        }

        public void MidiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            List<Byte> bytes = BitConverter.GetBytes(e.RawMessage).ToList();
            WriteLog(bytes, e.Timestamp);
        }

        public void WriteLog(List<Byte> bytes, int timestamp)
        {
            if (!DisableLog)
            {
                string bytesString = "";

                for (int i = 0; i < bytes.Count; i++)
                {
                    bytesString = string.Concat(bytesString, ", byte", i, ": ", bytes[i].ToString());
                }

                LogTextBoxText = string.Concat(LogTextBoxText, "\r\n", "Timestamp: ", timestamp, " ", bytesString);

                this.OnPropertyChanged("LogTextBoxText");
            }
        }
        #endregion Methods
    }
}
