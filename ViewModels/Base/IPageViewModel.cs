﻿namespace GuitarHeroDrums.ViewModels
{
    public interface IPageViewModel
    {
        System.Windows.Media.Brush ButtonBackground { get; }

        string Name { get; }

        void UpdateButtonBackground();
    }
}
