﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;

namespace GuitarHeroDrums.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public ViewModelBase(Window window)
        {
            this.UIDispatcher = window.Dispatcher;
            this.View = window;
        }

        #region Properties
        protected INotifyPropertyChanged DataModel { get; private set; }
        protected Dispatcher UIDispatcher { get; private set; }
        public Window View { get; private set; }
        #endregion Properties

        #region Methods
        private void DataModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnDataModelPropertyChanged(e.PropertyName);
        }

        protected virtual void OnDataModelPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                Action raiseEvent = () =>
                {
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    }
                };

                if (Dispatcher.CurrentDispatcher == this.UIDispatcher)
                    raiseEvent();
                else
                    this.UIDispatcher.Invoke(raiseEvent);
            }
        }

        protected void ReportError()
        {
            string errorLogPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "EDOCS Profile");
        }
        #endregion Methods
    }
}
