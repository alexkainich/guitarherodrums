﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using NAudio.Midi;
using GuitarHeroDrums.Utils;

namespace GuitarHeroDrums.ViewModels
{
    public class DevicesViewModel : ObservableObject
    {
        #region Fields
        private int _selectedMidiDeviceIndex;
        private bool _isKeyboardEnabled;
        private string _keyboardPreview;
        public MidiIn _midiIn;
        #endregion Fields

        public DevicesViewModel()
        {
            MidiSources = new ObservableCollection<string>();

            FindAvailableMidiDevices();

            IsKeyboardEnabled = true;
        }

        #region Propertied / Commands
        public ObservableCollection<string> MidiSources { get; set; }
        public int SelectedMidiDeviceIndex
        {
            get { return _selectedMidiDeviceIndex; }
            set
            {
                if (value != _selectedMidiDeviceIndex)
                {
                    _selectedMidiDeviceIndex = value;
                    this.OnPropertyChanged("SelectedMidiDeviceIndex");

                    MidiControl.ChangeDevice(_selectedMidiDeviceIndex);
                    if (IsKeyboardEnabled)
                    {
                        MidiControl.DisableKeyboard();
                        KeyboardPlaying.KeyPressed -= new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
                        MidiControl.EnableKeyboard();
                        KeyboardPlaying.KeyPressed += new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
                    }
                }
            }
        }
        public bool IsKeyboardEnabled
        {
            get { return _isKeyboardEnabled; }
            set
            {
                if (value != _isKeyboardEnabled)
                {
                    _isKeyboardEnabled = value;
                    if (_isKeyboardEnabled)
                    {
                        MidiControl.EnableKeyboard();
                        KeyboardPlaying.KeyPressed += new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
                    }
                    else
                    {
                        MidiControl.DisableKeyboard();
                        KeyboardPlaying.KeyPressed -= new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
                    }
                }
            }
        }

        public string KeyboardPreview
        {
            get { return _keyboardPreview; }
            set
            {
                _keyboardPreview = value;

                if (_keyboardPreview.Length > 300)
                {
                    _keyboardPreview = _keyboardPreview.Substring(100, _keyboardPreview.Length - 100);
                }
            }
        }

        private ICommand refreshDevices;
        public ICommand RefreshDevices
        {
            get
            {
                return this.refreshDevices ?? (this.refreshDevices = new RelayCommand(this.executeRefreshDevicesCommand, this.canRefreshDevicesCommand));
            }
        }

        private bool canRefreshDevicesCommand()
        {
            return true;
        }

        private void executeRefreshDevicesCommand()
        {
            try
            {
                FindAvailableMidiDevices();

                MidiControl.DisableKeyboard();
                KeyboardPlaying.KeyPressed -= new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
                MidiControl.EnableKeyboard();
                KeyboardPlaying.KeyPressed += new EventHandler<KeyboardPlaying.KeyEventArgs>(PreviewKeys);
            }
            catch
            {

            }
        }
        #endregion Propertied / commands

        #region Methods
        public void FindAvailableMidiDevices()
        {
            this.MidiSources.Clear();
            for (int device = 0; device < MidiIn.NumberOfDevices; device++)
            {
                this.MidiSources.Add(MidiIn.DeviceInfo(device).ProductName);
            }

            if (MidiSources.Any())
            {
                SelectedMidiDeviceIndex = 0;
                MidiControl.ChangeDevice(SelectedMidiDeviceIndex);
            }
        }

        public void PreviewKeys(object sender, KeyboardPlaying.KeyEventArgs e)
        {
            KeyboardPreview += e.KeyCombo;
            this.OnPropertyChanged("KeyboardPreview");
        }
        #endregion Methods
    }
}
