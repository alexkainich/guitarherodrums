﻿using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using GuitarHeroDrums.Models;
using GuitarHeroDrums.Styles;
using System;
using System.Windows.Data;

namespace GuitarHeroDrums.ViewModels
{
    public class SavedViewModel : ObservableObject, IPageViewModel
    {
        #region Fields
        private MessagesViewModel _messagesViewModel;
        #endregion Fields

        public SavedViewModel(MessagesViewModel messagesViewModel)
        {
            MessageViewItems = SavedKeys.SavedMidiInputs;

            //Enable the cross acces to this collection elsewhere
            //Fixed error "An ItemsControl is inconsistent with its items source." for executeDeleteSelectedCommand 
            BindingOperations.EnableCollectionSynchronization(MessageViewItems, _syncLock);

            Name = "Saved";
            _messagesViewModel = messagesViewModel;

            DeleteAllSavedVisibility = Visibility.Visible;

            CreateSavedView();

            SavedKeys.NotifyOnCollectionChanged(this);
        }

        #region Properties / Commands
        public string Name { get; set; }
        public System.Windows.Media.Brush ButtonBackground
        {
            get
            {
                var converter = new System.Windows.Media.BrushConverter();

                if (_messagesViewModel.CurrentPageViewModel.GetType() == typeof(SavedViewModel))
                {
                    return (System.Windows.Media.Brush)converter.ConvertFromString("#7398EC");
                }
                else
                {
                    return (System.Windows.Media.Brush)converter.ConvertFromString("#A9ABAF");
                }
            }
        }
        public double ScrollOffset { get; set; }
        public MidiInput SelectedMidiInput { get; set; }
        private static object _syncLock = new object();
        public ObservableCollection<MidiInput> MessageViewItems { get; set; }
        public ColumnConfig MessageViewItemsColumnConfig { get; set; }
        public Visibility DeleteAllSavedVisibility { get; set; }
       
        private ICommand deleteSelectedCommand;
        public ICommand DeleteSelectedCommand
        {
            get
            {
                return this.deleteSelectedCommand ?? (this.deleteSelectedCommand = new RelayCommand(this.executeDeleteSelectedCommand, this.canDeleteSelectedCommand));
            }
        }

        private bool canDeleteSelectedCommand()
        {
            return true;
        }

        private void executeDeleteSelectedCommand()
        {
            try
            {
                lock (_syncLock)
                {
                    SelectedMidiInput.AssignedKey = "";
                    MessageViewItems.Remove(SelectedMidiInput);

                    SavedKeys.SavedMidiInputs.Remove(SelectedMidiInput);
                    SavedKeys.SaveConfig();
                }         
            }
            catch
            {
                //Unhandled
            }
        }

        private ICommand deleteAllSavedCommand;
        public ICommand DeleteAllSavedCommand
        {
            get
            {
                return this.deleteAllSavedCommand ?? (this.deleteAllSavedCommand = new RelayCommand(this.executeDeleteAllSavedCommand, this.canDeleteAllSavedCommand));
            }
        }

        private bool canDeleteAllSavedCommand()
        {
            return true;
        }

        private void executeDeleteAllSavedCommand()
        {
            try
            {
                if (MessageViewItems.Any())
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure? This will delete all saves", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        MessageViewItems.ToList().ForEach(i => i.AssignedKey = "");
                        MessageViewItems.Clear();
                        SavedKeys.SaveConfig();
                    }
                }
            }
            catch
            {
                //Unhandled
            }
        }
        private ICommand assignKeyCommand;
        public ICommand AssignKeyCommand
        {
            get
            {
                return this.assignKeyCommand ?? (this.assignKeyCommand = new RelayCommand(this.executeAssignKeyCommand, this.canAssignKeyCommand));
            }
        }

        private bool canAssignKeyCommand()
        {
            return true;
        }

        private void executeAssignKeyCommand()
        {
            try
            {
                if (SelectedMidiInput != null)
                {
                    Recording.StartRecording(SelectedMidiInput);
                }
            }
            catch
            {

            }
        }
        #endregion Properties / Commands

        #region Methods
        public void UpdateButtonBackground()
        {
            OnPropertyChanged("ButtonBackground");
        }

        public void SavedMidiInputs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            CreateSavedView();
        }

        private void CreateSavedView()
        {
            if (SavedKeys.SavedMidiInputs.Any())
            {
                int midiBytesCount = SavedKeys.SavedMidiInputs.Max(i => i.Bytes.Count);

                List<int> byteIndicies = new List<int>();
                for (int i = 0; i < midiBytesCount; i++)
                {
                    byteIndicies.Add(i);
                }

                CreateMessageViewItemsColumns(byteIndicies);
            }
        }

        private void CreateMessageViewItemsColumns(List<int> byteIndicies)
        {
            List<Column> columns = new List<Column>();
            for (int i = 0; i < byteIndicies.Count; i++)
            {
                columns.Add(new Column { Header = string.Concat("Byte ", byteIndicies[i] + 1), DataField = string.Concat("Bytes[", byteIndicies[i], "]"), Width = "50" });
            }
            columns.Add(new Column { Header = "Assigned Key", DataField = "AssignedKey", Width = "300" });

            this.MessageViewItemsColumnConfig = new ColumnConfig { Columns = columns, };

            this.OnPropertyChanged("MessageViewItemsColumnConfig");
        }
        #endregion Methods
    }
}
