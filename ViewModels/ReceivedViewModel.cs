﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using NAudio.Midi;
using GuitarHeroDrums.Models;
using GuitarHeroDrums.Styles;

namespace GuitarHeroDrums.ViewModels
{
    public class ReceivedViewModel : ObservableObject, IPageViewModel
    {
        #region Fields
        private MidiInput _selectedMidiInput;
        private int _midiBytesCount = 0;
        private List<int> _filteredOutMidiBytes = new List<int>();
        private MessagesViewModel _messagesViewModel;
        #endregion Fields

        public ReceivedViewModel(MessagesViewModel messagesViewModel)
        {
            MessageViewItems = new ObservableCollection<MidiInput>();
            Name = "All";
            _messagesViewModel = messagesViewModel;

            ColumnControlsVisibility = Visibility.Visible;
        }

        #region Properties / Commands
        public string Name { get; set; }
        public System.Windows.Media.Brush ButtonBackground
        {
            get
            {
                var converter = new System.Windows.Media.BrushConverter();

                if (_messagesViewModel.CurrentPageViewModel.GetType() == typeof(ReceivedViewModel))
                {
                    return (System.Windows.Media.Brush)converter.ConvertFromString("#7398EC");
                }
                else
                {
                    return (System.Windows.Media.Brush)converter.ConvertFromString("#A9ABAF");
                }
            }
        }
        public double ScrollOffset { get; set; }
        public ObservableCollection<MidiInput> MessageViewItems { get; set; }
        public ColumnConfig MessageViewItemsColumnConfig { get; set; }
        public MidiInput SelectedMidiInput
        {
            get { return _selectedMidiInput; }
            set
            {
                if (value != _selectedMidiInput)
                {
                    Recording.StopRecordingAndAssignKey("");
                    _selectedMidiInput = value;
                }
            }
        }
        public Visibility ColumnControlsVisibility { get; set; }

        private ICommand resetAvailableColumns;
        public ICommand ResetAvailableColumns
        {
            get
            {
                return this.resetAvailableColumns ?? (this.resetAvailableColumns = new RelayCommand(this.executeResetAvailableColumnsCommand, this.canResetAvailableColumnsCommand));
            }
        }

        private bool canResetAvailableColumnsCommand()
        {
            return true;
        }

        private void executeResetAvailableColumnsCommand()
        {
            try
            {
                List<int> byteIndicies = new List<int>();

                for (int i = 0; i < _midiBytesCount; i++) { byteIndicies.Add(i); }
                ResetAvailableMidiInputColumns(byteIndicies);
            }
            catch
            {

            }
        }

        private ICommand clearAllAvailableMidiInputItemsCommand;
        public ICommand ClearAllAvailableMidiInputItemsCommand
        {
            get
            {
                return this.clearAllAvailableMidiInputItemsCommand ?? (this.clearAllAvailableMidiInputItemsCommand = new RelayCommand(this.executeclearAllAvailableMidiInputItemsCommand, this.canclearAllAvailableMidiInputItemsCommand));
            }
        }

        private bool canclearAllAvailableMidiInputItemsCommand()
        {
            return true;
        }

        private void executeclearAllAvailableMidiInputItemsCommand()
        {
            try
            {
                MessageViewItems.Clear();
            }
            catch
            {

            }
        }

        private ICommand assignKeyCommand;
        public ICommand AssignKeyCommand
        {
            get
            {
                return this.assignKeyCommand ?? (this.assignKeyCommand = new RelayCommand(this.executeAssignKeyCommand, this.canAssignKeyCommand));
            }
        }

        private bool canAssignKeyCommand()
        {
            return true;
        }

        private void executeAssignKeyCommand()
        {
            try
            {
                if (SelectedMidiInput != null)
                {
                    Recording.StartRecording(SelectedMidiInput);
                }
            }
            catch
            {

            }
        }
        #endregion Propertied / Commands

        #region Methods
        public void UpdateButtonBackground()
        {
            OnPropertyChanged("ButtonBackground");
        }

        public void MidiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            List<Byte> bytes = BitConverter.GetBytes(e.RawMessage).ToList();

            ResetMidiBytesCount(bytes);

            AddToMessageViewItems(bytes);
        }

        private void ResetMidiBytesCount(List<Byte> bytes)
        {
            if (_midiBytesCount != bytes.Count)
            {
                _midiBytesCount = bytes.Count;

                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    List<int> byteIndicies = new List<int>();

                    for (int i = 0; i < bytes.Count; i++)
                    {
                        byteIndicies.Add(i);
                    }

                    ResetAvailableMidiInputColumns(byteIndicies);
                });
            }
        }

        private void AddToMessageViewItems(List<Byte> bytes)
        {
            List<Byte?> filteredBytes = FilterBytes(bytes, _filteredOutMidiBytes);

            string name = string.Join(",", filteredBytes);
            bool itemExistsInList = MessageViewItems.Where(i => i.Name == name).Any();
            if (!itemExistsInList)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    var itemExistsInSavedKeys = SavedKeys.SavedMidiInputs.Where(i => i.Name == name).FirstOrDefault();
                    if (itemExistsInSavedKeys != null)
                    {
                        MessageViewItems.Add(itemExistsInSavedKeys);
                    }              
                    else
                    {
                        MessageViewItems.Add(new MidiInput(filteredBytes));
                    }
                });
            }
        }

        private void ResetAvailableMidiInputColumns(List<int> byteIndicies)
        {
            CreateMessageViewItemsColumns(byteIndicies);
            _filteredOutMidiBytes.Clear();
            byteIndicies.ForEach(b => _filteredOutMidiBytes.Add(1));
        }

        private List<Byte?> FilterBytes(List<Byte>bytes, List<int> filter)
        {
            List<Byte?> filteredBytes = new List<Byte?>();
            if (filter != null)
            {
                for (int i = 0; i < bytes.Count; i++)
                {
                    filteredBytes.Add(filter[i] == 1 ? (Byte?)bytes[i] : null);
                }
            }
            else
            {
                bytes.ForEach(b => filteredBytes.Add(b));
            }

            return filteredBytes;
        }

        public void CreateMessageViewItemsColumns(List<int> byteIndicies)
        {
            List<Column> columns = new List<Column>();
            for (int i = 0; i < byteIndicies.Count; i++)
            {
                columns.Add(new Column { Header = string.Concat("Byte ", byteIndicies[i] + 1), DataField = string.Concat("Bytes[", byteIndicies[i], "]"), Width = "50" });
            }
            columns.Add(new Column { Header = "Assigned Key", DataField = "AssignedKey", Width = "300" });

            this.MessageViewItemsColumnConfig = new ColumnConfig { Columns = columns, };

            this.OnPropertyChanged("MessageViewItemsColumnConfig");
        }

        //Only used internally by the code behind
        public void FilterOutColumn(string columnNameClicked)
        {
            if(columnNameClicked == "Assigned Key")
            {
                return;
            }

            //Add to FilteredOutMidiBytes list
            int byteIndexToFilterOut = Int32.Parse(columnNameClicked.Substring(columnNameClicked.IndexOf("Byte ") + 5)) - 1;
            _filteredOutMidiBytes[byteIndexToFilterOut] = 0;

            //Create new columns in the listview
            List<int> byteIndicies = new List<int>();
            for (int i = 0; i < _midiBytesCount; i++)
            {
                if (_filteredOutMidiBytes[i] == 1) { byteIndicies.Add(i); }
            }
            CreateMessageViewItemsColumns(byteIndicies);

            //Remove filtered out bytes from all items in MessageViewItems
            //List<MidiInput> filteredMessageViewItems = new List<MidiInput>(MessageViewItems);
            List<MidiInput> filteredMessageViewItems = new List<MidiInput>();
            MessageViewItems.ToList().ForEach(i => filteredMessageViewItems.Add(new MidiInput(i.Bytes.Select(b => (Byte?)b).ToList(), i.AssignedKey)));

            for (int i = 0; i < filteredMessageViewItems.Count; i++)
            {
                for (int j = 0; j < filteredMessageViewItems[i].Bytes.Count; j++)
                {
                    if (_filteredOutMidiBytes[j] == 0)
                    {
                        filteredMessageViewItems[i].Bytes[j] = null;
                    }
                }

                filteredMessageViewItems[i].Name = string.Join(",", filteredMessageViewItems[i].Bytes);
            }

            //Leave only distinct items in MessageViewItems and assign keys
            var newMessageViewItems = new ObservableCollection<MidiInput>();
            foreach (MidiInput item in filteredMessageViewItems)
            {
                if (!newMessageViewItems.Where(i => i.Name == item.Name).Any())
                {
                    item.AssignedKey = "";
                    if (SavedKeys.SavedMidiInputs != null && SavedKeys.SavedMidiInputs.Any())
                    {
                        MidiInput savedMidiInput = SavedKeys.SavedMidiInputs.Where(s => s.Name == item.Name).FirstOrDefault();
                        if (savedMidiInput != null)
                        {
                            newMessageViewItems.Add(savedMidiInput);
                            continue;
                        }
                    }

                    newMessageViewItems.Add(item);
                }
            }

            MessageViewItems = newMessageViewItems;
            OnPropertyChanged("MessageViewItems");
        }
        #endregion Methods
    }
}
