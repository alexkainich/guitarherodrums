﻿using System;
using GuitarHeroDrums.Models;
using GuitarHeroDrums.Utils;

namespace GuitarHeroDrums.ViewModels
{
    public partial class MainViewModel : ObservableObject
    {
        public MainViewModel()
        {
            //Load saved midi keys
            SavedKeys.ReadUserConfig();

            //Prepare messages screen
            MessagesViewModel = new MessagesViewModel();

            //Prepare log screen
            LogViewModel = new LogViewModel();

            //Search for midi devices and activate the first one found.
            MidiControl.SubscribeVMs(new Object[] { MessagesViewModel, LogViewModel });

            //Prepare the devices screen
            DevicesViewModel = new DevicesViewModel();
        }

        #region Properties
        public LogViewModel LogViewModel { get; set; }
        public MessagesViewModel MessagesViewModel { get; set; }
        public DevicesViewModel DevicesViewModel { get; set; }
        #endregion Properties
    }
}