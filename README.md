# Guitar Hero Drums

## Guitar Hero Drums is a flexible midi to keyboard mapper

### Guitar Hero Drums features:
- Allows you to play Guitar Hero by using any midi drum kit !

**Also:**
- Recognises input from any midi device connected to the PC. Examples of such devices are midi controllers and electric midi drum kits.
- Provides detailed information on each signal received.
- Map any signal to any keyboard shortcut. Now your midi device can be used to control your PC !
- Can be used  to send midi input to Windows applications and PC games.
- The application automatically saves and restores user changes.

### Guitar Hero Drums Screenshots
<img src="ReadmeImages/GuitarHeroDrums1.jpg" width="500" height="600">
<img src="ReadmeImages/GuitarHeroDrums2.jpg" width="500" height="600">

### Guitar Hero Drums User Guide
**Preparation:**
- Plug in your midi drums and install drivers. Get a copy of Guitar Hero (World Tour is a good choice) and make sure everything is working. Don't forget to install this app too, by running the .msi file !

**Use:**
1. Run the .exe and start using your midi device. If everything has gone well you should see various midi inputs (the received midi messages as a byte sequence)  appearing on the window.
2. Use your midi device and see all the various midi inputs appearing on the window.
3. Select one of them by double clicking on it.
4. You now have 5 seconds to type a keyboard shortcut (Assigned key) once. This will be recorded. You can only use up to a combination of 4 keys.
5. Now every time you play the midi input in step 3, the recorded Assigned key will be triggered.

**Tips:**
- In the Devices section there is a drop down menu of available devices. If the list is empty then please see the Troubleshooting section of this readme file.
- You can see a list of the changes you made in the "Saved" window (click the "Saved" button in the "Messages" section.
- As the different midi inputs may be too many at start. Try to filter them by clicking on the different byte columns in the "Messages" section list. This will filter out the clicked byte from the received midi sequence. For example, many application use the third byte as an indication of "speed". Also from left --> right bytes become less generic when describing the midi input.
- You could trigger a keyboard shortcut / Assigned key on ANY midi input received by clicking on all the byte columns and assigning a new key.

**Troubleshooting:**
- First make sure that the application runs (a window should appear with the title "Guitar Hero Drums").
- Then make sure there are devices listed in the drop down menu under the "Devices" section. If there are none, then  please check that your midi device is properly connected to your PC, that the drivers are installed and that the midi device is recognised by Microsoft Windows and ready to use.
- There is also a "Refresh" button in the "Devices" section. This will try to refresh the devices.
