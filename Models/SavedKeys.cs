﻿using System.Linq;
using System.Configuration;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using GuitarHeroDrums.ViewModels;

namespace GuitarHeroDrums.Models
{
    public class SavedKeys
    {
        #region Fields / Properties
        private static Configuration _config = ConfigurationManager.OpenExeConfiguration(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static ObservableCollection<MidiInput> SavedMidiInputs { get; set; }
        #endregion Fields / Properties

        #region Methods
        public static void SaveItem(MidiInput midiInput)
        {
            if (!string.IsNullOrEmpty(midiInput.AssignedKey))
            {
                AddToSavedMidiInputs(midiInput);

                SavedMidiInputs.First(i => i.Name == midiInput.Name).AssignedKey = midiInput.AssignedKey;

                SaveConfig();
            }
        }

        public static void ReadUserConfig()
        {
            ObservableCollection<MidiInput> results = new ObservableCollection<MidiInput>();

            var savedMidiInputs = _config.AppSettings.Settings["SavedMidiInputs"];
            if (savedMidiInputs != null)
            {
                string userConfig = savedMidiInputs.Value;
                results = JsonConvert.DeserializeObject<ObservableCollection<MidiInput>>(userConfig);
            }
            else
            {
                results = new ObservableCollection<MidiInput>();
            }

            SavedMidiInputs = results;
        }

        public static void SaveConfig()
        {
            string savedMidiInputsJSON = JsonConvert.SerializeObject(SavedMidiInputs);
            _config.AppSettings.Settings.Remove("SavedMidiInputs");
            _config.AppSettings.Settings.Add("SavedMidiInputs", savedMidiInputsJSON);
            _config.Save(ConfigurationSaveMode.Minimal);
        }

        public static void NotifyOnCollectionChanged(object vm)
        {
            if (vm.GetType() == typeof(SavedViewModel))
            {
                SavedViewModel savedViewModel = (SavedViewModel)vm;
                SavedMidiInputs.CollectionChanged += savedViewModel.SavedMidiInputs_CollectionChanged;
            }
        }

        private static void AddToSavedMidiInputs(MidiInput midiInput)
        {
            bool midiInputAlreadySaved = SavedMidiInputs == null ? false : SavedMidiInputs.Where(x => x.Name == midiInput.Name).Any();
            if (!midiInputAlreadySaved)
            {
                SavedMidiInputs.Add(midiInput);
            }
        }
        #endregion Methods
    }
}
