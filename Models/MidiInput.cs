﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace GuitarHeroDrums.Models
{
    public class MidiInput : INotifyPropertyChanged
    {
        public MidiInput(List<Byte?> bytes, string assignedKey = "")
        {
            this.Bytes = bytes;
            this.AssignedKey = assignedKey;
            this.Name = string.Join(",", bytes);
        }

        #region Properties
        public List<Byte?> Bytes { get; set; }
        public string Name { get; set; }

        private string assignedKey;
        public string AssignedKey
        {
            get { return assignedKey; }
            set
            {
                assignedKey = value;
                this.OnPropertyChanged("AssignedKey");
            }
        }
        #endregion Properties

        #region Event
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion Event
    }
}
