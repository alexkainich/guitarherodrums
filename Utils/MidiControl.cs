﻿using GuitarHeroDrums.ViewModels;
using NAudio.Midi;
using System;
namespace GuitarHeroDrums.Utils
{
    public static class MidiControl
    {
        #region Fields
        private static object[] _viewModels;
        public static MidiIn _midiIn;
        #endregion Fields

        #region Public Methods
        public static void SubscribeVMs(object[] vm)
        {
            _viewModels = vm;
        }

        public static void ChangeDevice(int selectedMidiDeviceIndex)
        {
            try
            {
                //stop current active midi device
                if (_midiIn != null)
                {
                    _midiIn.Stop();
                    _midiIn.Dispose();

                    UnsubscribeAllEvents();
                }
            }
            catch
            {
                //midi is unplagged or may need restarting
            }

            try
            {
                //start new selected midi device
                if (selectedMidiDeviceIndex >= 0)
                {
                    _midiIn = new MidiIn(selectedMidiDeviceIndex);
                    _midiIn.Start();

                    SubscribeAllEvents();
                }
                else
                {
                    _midiIn = null;
                }
            }
            catch
            {
                //midi is unplagged
                return;
            }
        }

        public static void EnableKeyboard()
        {
            if (_midiIn != null)
                _midiIn.MessageReceived += new EventHandler<MidiInMessageEventArgs>(KeyboardPlaying.MidiIn_MessageReceived);
        }

        public static void DisableKeyboard()
        {
            if (_midiIn != null)
                _midiIn.MessageReceived -= new EventHandler<MidiInMessageEventArgs>(KeyboardPlaying.MidiIn_MessageReceived);
        }
        #endregion Public Methods

        #region Private Methods
        private static void SubscribeAllEvents()
        {
            foreach (var vm in _viewModels)
            {
                if (vm.GetType() == typeof(LogViewModel))
                {
                    LogViewModel logViewModel = (LogViewModel)vm;
                    _midiIn.MessageReceived += new EventHandler<MidiInMessageEventArgs>(logViewModel.MidiIn_MessageReceived);
                }
                else if (vm.GetType() == typeof(MessagesViewModel))
                {
                    MessagesViewModel messagesViewModel = (MessagesViewModel)vm;
                    if (messagesViewModel.CurrentPageViewModel.GetType() == typeof(ReceivedViewModel))
                    {
                        ReceivedViewModel receivedViewModel = (ReceivedViewModel)messagesViewModel.CurrentPageViewModel;
                        _midiIn.MessageReceived += new EventHandler<MidiInMessageEventArgs>(receivedViewModel.MidiIn_MessageReceived);
                    }
                }
            }
        }

        private static void UnsubscribeAllEvents()
        {
            foreach (var vm in _viewModels)
            {
                if (vm.GetType() == typeof(LogViewModel))
                {
                    LogViewModel logviewModel = (LogViewModel)vm;
                    _midiIn.MessageReceived -= new EventHandler<MidiInMessageEventArgs>(logviewModel.MidiIn_MessageReceived);
                }
                else if (vm.GetType() == typeof(MessagesViewModel))
                {
                    MessagesViewModel messagesViewModel = (MessagesViewModel)vm;
                    if (messagesViewModel.CurrentPageViewModel.GetType() == typeof(ReceivedViewModel))
                    {
                        ReceivedViewModel receivedViewModel = (ReceivedViewModel)messagesViewModel.CurrentPageViewModel;
                        _midiIn.MessageReceived -= new EventHandler<MidiInMessageEventArgs>(receivedViewModel.MidiIn_MessageReceived);
                    }
                }
            }
        }
        #endregion Private Methods
    }
}
