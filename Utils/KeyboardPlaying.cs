﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Runtime.InteropServices;
using NAudio.Midi;
using GuitarHeroDrums.Models;
using System.Windows.Forms;

namespace GuitarHeroDrums.Utils
{
    public static class KeyboardPlaying
    {
        #region Methods
        public static void MidiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            List<Byte> bytes = BitConverter.GetBytes(e.RawMessage).ToList();

            List<MidiInput> midiInputs = GetTriggeredMidiInputs(bytes);
            foreach (MidiInput midiInput in midiInputs)
            {
                string[] stringKeys = midiInput.AssignedKey.Split('+');

                System.Windows.Forms.KeysConverter kc = new System.Windows.Forms.KeysConverter();
                Keys[] keys = new Keys[stringKeys.Length];
                for (int i = 0; i < stringKeys.Length; i++)
                {
                    keys[i] = (Keys)kc.ConvertFromString(stringKeys[i]);
                }

                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    PressKeys(keys, midiInput.AssignedKey);
                }).Start();
            }
        }

        private static List<MidiInput> GetTriggeredMidiInputs(List<Byte> bytes)
        {
            List<MidiInput> results = new List<MidiInput>();

            foreach(MidiInput midiInput in SavedKeys.SavedMidiInputs)
            {
                int counter = 0;
                for (int i = 0; i < bytes.Count; i++)
                {
                    if (bytes[i] == midiInput.Bytes[i] || midiInput.Bytes[i] == null)
                    {
                        counter++;
                    }
                }

                if (counter == bytes.Count)
                {
                    results.Add(midiInput);
                }
            }

            return results;
        }

        private static void PressKeys(Keys[] keys, string assignedKey)
        {
            try
            {
                int keysDownCount = 0;
                int keysUpCount = 0;

                //Keys down
                for (int i = 0; i < keys.Count(); i++)
                {
                    keysDownCount = (int)DirectInput.Send_Key((short)DirectInput.MapVirtualKey((uint)keys[i], 0), 0 | 8); //keydown | scancode
                }

                Thread.Sleep(100);

                //Keys up
                for (int i = 0; i < keys.Count(); i++)
                {
                    keysUpCount = (int)DirectInput.Send_Key((short)DirectInput.MapVirtualKey((uint)keys[i], 0), 2 | 8); //keyup | scancode
                }

                //If everything went well then raise event to update the keyboard preview
                if (keys.Count() == keysDownCount && keysDownCount == keysUpCount)
                {
                    OnKeyPressed(new KeyEventArgs(assignedKey));
                }
            }
            catch
            {
                //Unhandled - TODO: Should display a message
            }
        }


        private static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        #endregion Methods

        #region Events
        public static event EventHandler<KeyEventArgs> KeyPressed;
        public static void OnKeyPressed(KeyEventArgs e)
        {
            KeyPressed?.Invoke(null, e);
        }

        public class KeyEventArgs : EventArgs
        {
            public KeyEventArgs(string keyCombo)
            {
                this.KeyCombo = keyCombo;
            }
            public string KeyCombo { get; private set; }
        }
        #endregion Events
    }

    public class DirectInput
    {
        [DllImport("user32.dll")]
        static extern UInt32 SendInput(UInt32 nInputs, [MarshalAs(UnmanagedType.LPArray, SizeConst = 1)] INPUT[] pInputs, Int32 cbSize);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, uint uMapType);

        [StructLayout(LayoutKind.Sequential)]
        struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int dwFlags;
            public int time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct KEYBDINPUT
        {
            public short wVk;
            public short wScan;
            public int dwFlags;
            public int time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct HARDWAREINPUT
        {
            public int uMsg;
            public short wParamL;
            public short wParamH;
        }

        [StructLayout(LayoutKind.Explicit)]
        struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(4)]
            public MOUSEINPUT mi;
            [FieldOffset(4)]
            public KEYBDINPUT ki;
            [FieldOffset(4)]
            public HARDWAREINPUT hi;
        }

        const int KEYEVENTF_EXTENDEDKEY = 0x0001;
        const int KEYEVENTF_KEYUP = 0x0002;
        const int KEYEVENTF_UNICODE = 0x0004;
        const int KEYEVENTF_SCANCODE = 0x0008;

        public static uint Send_Key(short Keycode, int KeyUporDown)
        {
            INPUT[] InputData = new INPUT[1];

            InputData[0].type = 1;
            InputData[0].ki.wScan = Keycode;
            InputData[0].ki.dwFlags = KeyUporDown;
            InputData[0].ki.time = 0;
            InputData[0].ki.dwExtraInfo = IntPtr.Zero;

            return SendInput(1, InputData, Marshal.SizeOf(typeof(INPUT)));
        }
    }
}
