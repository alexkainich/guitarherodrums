﻿using GuitarHeroDrums.Utils;
using System;
using System.Collections.Generic;
namespace GuitarHeroDrums.Models
{
    public static class Recording
    {
        #region Fields
        private static int _recordingTime = 5;
        private static int _recordingTimeCurrent = 5;

        private static bool _isRecording = false;
        private static System.Windows.Threading.DispatcherTimer _dispatcherTimer;
        #endregion Fields

        #region Properties
        private static MidiInput SelectedMidiInput { get; set; }
        #endregion Properties

        #region Methods
        public static void StartRecording(MidiInput selectedMidiInput)
        {
            if (!_isRecording)
            {
                SelectedMidiInput = selectedMidiInput;

                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    _isRecording = true;
                    _recordingTimeCurrent = _recordingTime;

                    KeyboardRecording.Start();
                    KeyboardRecording.KeyboardAction += new EventHandler<KeyboardEventArgs>(OnKeyReceived);

                    UpdateRecordingMessage();
                });
            }
            else
            {
                throw new Exception("Tried to start new recording while already recording");
            }
        }

        public static void UpdateRecordingMessage(object sender = null, EventArgs e = null)
        {
            if (_dispatcherTimer != null)
                _dispatcherTimer.Stop();

            if (_isRecording)
            {
                if (_recordingTimeCurrent == 0)
                {
                    StopRecordingAndAssignKey("");

                    return;
                }

                SelectedMidiInput.AssignedKey = "Waiting for key input " + _recordingTimeCurrent + "...";

                _recordingTimeCurrent--;
                _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                _dispatcherTimer.Tick += UpdateRecordingMessage;
                _dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                _dispatcherTimer.Start();
            }
        }

        public static void StopRecordingAndAssignKey(string assignedKey = null)
        {
            if (_isRecording)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    if (assignedKey != null)
                    {
                        SelectedMidiInput.AssignedKey = assignedKey;
                    }

                    _isRecording = false;

                    KeyboardRecording.Stop();
                    KeyboardRecording.KeyboardAction -= new EventHandler<KeyboardEventArgs>(OnKeyReceived);
                });
            }
        }

        private static void OnKeyReceived(object sender, KeyboardEventArgs e)
        {
            if (_isRecording)
            {
                String[] substrings = e.KeyCombo.Split(':');
                if (substrings[0] == "recording")
                {
                    StopRecordingAndAssignKey(substrings[1]);

                    SavedKeys.SaveItem(SelectedMidiInput);
                }
            }
        }
        #endregion Methods
    }
}
