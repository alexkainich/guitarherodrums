﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GuitarHeroDrums.ViewModels;

namespace GuitarHeroDrums.Views
{
    public partial class MessagesListView : UserControl
    {
        public MessagesListView()
        {
            InitializeComponent();
        }

        private void SaveScrollOffset(object sender, DependencyPropertyChangedEventArgs e)
        {
            object oldVM = e.OldValue;
            ScrollViewer _scrollViewer = FindScrollViewer(listView);

            if (oldVM != null)
            {
                if (oldVM.GetType() == typeof(ReceivedViewModel))
                {
                    ReceivedViewModel vm = (ReceivedViewModel)oldVM;
                    vm.ScrollOffset = _scrollViewer.VerticalOffset;
                }
                else if (oldVM.GetType() == typeof(SavedViewModel))
                {
                    SavedViewModel vm = (SavedViewModel)oldVM;
                    vm.ScrollOffset = _scrollViewer.VerticalOffset;
                }
            }
        }

        private void SetScrollOffset(object sender, RoutedEventArgs e)
        {
            ScrollViewer _scrollViewer = FindScrollViewer(listView);

            if (_scrollViewer != null)
            {
                if (this.DataContext.GetType() == typeof(ReceivedViewModel))
                {
                    ReceivedViewModel vm = (ReceivedViewModel)this.DataContext;
                    _scrollViewer.ScrollToVerticalOffset(vm.ScrollOffset);
                }
                else if (this.DataContext.GetType() == typeof(SavedViewModel))
                {
                    SavedViewModel vm = (SavedViewModel)this.DataContext;
                    _scrollViewer.ScrollToVerticalOffset(vm.ScrollOffset);
                }
            }
        }

        private ScrollViewer FindScrollViewer(DependencyObject d)
        {
            if (d is ScrollViewer)
                return d as ScrollViewer;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(d); i++)
            {
                var sw = FindScrollViewer(VisualTreeHelper.GetChild(d, i));
                if (sw != null) return sw;
            }
            return null;
        }

        private void FilterOutColumn(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader columnClicked = e.OriginalSource as GridViewColumnHeader;

            if (columnClicked != null && columnClicked.Content != null)
            {
                string columnNameClicked = columnClicked.Content.ToString();

                if (this.DataContext.GetType() == typeof(ReceivedViewModel))
                {
                    ReceivedViewModel vm = (ReceivedViewModel)this.DataContext;
                    vm.FilterOutColumn(columnNameClicked);
                }
            }
        }

        // Disable listview keyboard navigation
        private void ListView_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }
    }
}
